properties(
    [parameters([
        choice(
            choices: ['dev', 'qa', 'perf', 'staging', 'prod'],
            defaultValue: 'dev',
            description: 'Target AWS environment',
            name: 'awsEnvironment'
        ),
        string(
            description: 'Arn of snapshot to be used to create database',
            name: 'snapshotIdentifier'
        ),
        booleanParam(
            description: 'Restore using snapshot?',
            name: 'useSnapshot'
        ),
        booleanParam(
            description: 'Run Liquibase?',
            name: 'runLiquibase'
        ),
        string(
            description: 'Liquibase JAR version to deploy post-infra build',
            name: 'artifactVersionNumber'
        )
    ]),
    pipelineTriggers([ ])
])

def dbSnapshot
if (("${useSnapshot}" != "false") && ("${snapshotIdentifier}" != "" )) {
    dbSnapshot = "${snapshotIdentifier}"
}
else {
    dbSnapshot = "none"
}

@Library('jenkins-shared-library@master') _
pipeline_oracle_postgres_cft_deployment(
    awsEnvironment: "${awsEnvironment}",
    artifactVersionNumber: "${artifactVersionNumber}",
    baseStackName: "olscore-postgres",
    liquibaseRun: "${runLiquibase}",
    liquibaseDeployJobName: "OLSCORE/Schema/olscore-schema-deploy/development",
    stackName: "${awsEnvironment}-olscore-aurora-db",
    templatePath: "infrastructure.yml",
    application: "olscore-aurora-db",
    applicationFamily: "ols-platform",
    parameters: //Change to be database specific as needed
    [
        DatabaseName:"${environment_db_name("$awsEnvironment")}",
        Environment: "${awsEnvironment}",
        DatabaseSnapshotId: "${dbSnapshot}"
    ],
    region: "us-east-1"
)

def environment_db_name(awsEnvironment) {
    def value
    switch(awsEnvironment) {
        case "dev":
            value = "DEVOLSCORE"
            break
        case "qa":
            value = "QAOLSCORE"
            break
        case "perf":
            value = "PERFOLSCORE"
            break
        case "staging":
            value = "STGOLSCORE"
            break
        case "prod":
            value = "OLSCORE"
            break
    }
    return value
}